const { src, dest, series } = require('gulp');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const minifyHtml = require('gulp-minify-html');

function jsMin() {
	return src('original/public/**/*.js').pipe(uglify()).pipe(dest('dist/'));
}

function cssMin() {
	return src('original/public/**/*.css').pipe(cleanCSS()).pipe(dest('dist/'));
}

function htmlMin() {
	return src('original/public/**/*.html').pipe(minifyHtml()).pipe(dest('dist/'));
}

exports.js = jsMin;
exports.css = cssMin;
exports.html = htmlMin;
exports.default = series(jsMin, cssMin, htmlMin);